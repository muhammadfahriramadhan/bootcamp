package com.sudomakmur.bootcamp

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentResultListener
import androidx.fragment.app.setFragmentResultListener
import kotlinx.android.synthetic.main.fragment_two.*


class TwoFragment : Fragment() {
    private val TAG = "TwoFragment"
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_two, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setFragmentResultListener("from_fragment_1") { requestKey, bundle ->
             Log.i(TAG,bundle.getString("value_edittext")!!)
            textFragment.text  = bundle.getString("value_edittext")
        }


    }

}