package com.sudomakmur.bootcamp.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
class Register2(val username : String,val email :String, val password : String) : Parcelable