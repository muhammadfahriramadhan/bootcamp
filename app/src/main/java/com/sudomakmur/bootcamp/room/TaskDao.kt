package com.sudomakmur.bootcamp.room

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy.REPLACE
import androidx.room.Query
import com.sudomakmur.bootcamp.model.TaskEntity

@Dao
interface TaskDao {

    @Query("SELECT * FROM TaskEntity ")
    fun getAllTask() : List<TaskEntity>

    @Insert(onConflict = REPLACE)
    fun insertTask(taskEntity : TaskEntity)  : Long
}