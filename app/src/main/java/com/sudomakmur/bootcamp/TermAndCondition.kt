package com.sudomakmur.bootcamp

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.widget.Toast
import androidx.annotation.StringRes
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.sudomakmur.bootcamp.Constant.DATA_DARI_REGISTER
import com.sudomakmur.bootcamp.model.Register
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_term_and_condition.*
import permissions.dispatcher.*

@RuntimePermissions
class TermAndCondition : AppCompatActivity() {

    private var sharedPref : SharedPreferences?= null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_term_and_condition)

//        webViewTermAndCondition.loadUrl("files:///android_asset/TermAndCondition.html")

//        webViewTermAndCondition.loadUrl("file:///android_asset/TermAndCondition.html")


//        val dataRegister = intent.getParcelableExtra<Register>(DATA_DARI_REGISTER)
        sharedPref = getSharedPreferences("register",Context.MODE_PRIVATE)
        val username =  sharedPref?.getString("username","tidak ada username")
        val email  = sharedPref?.getString("email","tidak ada email")
        val password  = sharedPref?.getString("password","tidak ada password")



        textValueUserName.text  = username
        textValueEmail.text = email
        textValueUserPassword.text = password



        Glide.with(this)
                .load("https://openthread.google.cn/images/ot-contrib-google.png")
                .into(imgProfile)

        onLocationAndStorage()
    }


    @NeedsPermission(Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.WRITE_EXTERNAL_STORAGE)
    fun onLocationAndStorage() {

    }

    @OnShowRationale(Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.WRITE_EXTERNAL_STORAGE)
    fun showRationaleForLocationAndStorage(request: PermissionRequest) {
        showRationaleDialog(this,R.string.lokasi_storage, request)
    }

    @OnPermissionDenied(Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.WRITE_EXTERNAL_STORAGE)
    fun onLocationAndStorageDenied() {
        Toast.makeText(this,"Storage and Location Denied", Toast.LENGTH_SHORT).show()
    }

    @OnNeverAskAgain(Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.WRITE_EXTERNAL_STORAGE)
    fun onLocationAndStorageNeverAskAgain() {
        Toast.makeText(this, "Storage and Location Denie", Toast.LENGTH_SHORT).show()
    }


    fun showRationaleDialog(context: Context, @StringRes message: Int, permission: PermissionRequest) {
        AlertDialog.Builder(context)
                .setTitle("allow permission")
                .setPositiveButton("allow") { _, _ -> permission.proceed() }
                .setCancelable(false)
                .setMessage(message)
                .show()
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        // NOTE: delegate the permission handling to generated function
        onRequestPermissionsResult(requestCode,permissions, grantResults)
    }
}