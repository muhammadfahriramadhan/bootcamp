package com.sudomakmur.bootcamp

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.google.android.material.tabs.TabLayoutMediator
import kotlinx.android.synthetic.main.activity_fragment.*

class FragmentActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_fragment)
        val title = arrayListOf<String>("Calls","Chat")

        viewPager2.adapter = PagerAdapterSlide(this)

        TabLayoutMediator(tab_layout,viewPager2){ tab,position ->
            tab.text = title.get(position)
        }.attach()

    }


    fun onNext(){
        viewPager2.setCurrentItem(viewPager2.currentItem+1,true)
    }

    inner class PagerAdapterSlide(fa : FragmentActivity) : FragmentStateAdapter(fa){

        override fun getItemCount(): Int {
            return NUM_PAGES
        }

        override fun createFragment(position: Int): Fragment {
            when(position){
                0 -> return HelloFragment()
                1 -> return TwoFragment()
            }
            return  HelloFragment()
        }


    }

    companion object{
         const val NUM_PAGES = 2
    }
}

