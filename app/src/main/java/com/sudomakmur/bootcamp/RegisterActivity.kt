package com.sudomakmur.bootcamp

import android.app.AlertDialog
import android.app.DatePickerDialog
import android.app.DatePickerDialog.OnDateSetListener
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.RadioGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.snackbar.Snackbar
import com.sudomakmur.bootcamp.Constant.DATA_DARI_LOGIN
import com.sudomakmur.bootcamp.Constant.DATA_DARI_REGISTER
import com.sudomakmur.bootcamp.model.Register
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.custom_dialog.*
import kotlinx.android.synthetic.main.custom_dialog.view.*
import java.text.SimpleDateFormat
import java.util.*




class RegisterActivity : AppCompatActivity(),AdapterView.OnItemSelectedListener{

    var myCalendar : Calendar ? = null

    val dataDariLogin : Int? by lazy {

        intent.getIntExtra(DATA_DARI_LOGIN,0)
    }
    private var sharedPref : SharedPreferences?= null



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        onRadioButtonCliked()
        onSpinnerAttach()
        onBirthdateSeleted()

        sharedPref = getSharedPreferences("register",Context.MODE_PRIVATE)
        editTextUsername.setText(dataDariLogin.toString())

        checkTermCondition.setOnClickListener {
//            finish()
//            val i = Intent(this,TermAndCondition::class.java)
//            val dataRegister = Register(editTextUsername.text.toString(),editTextEmail.text.toString(),editTextPassword.text.toString())
//            i.putExtra(DATA_DARI_REGISTER,dataRegister)
//            startActivity(i)
        }


        btnRegiterAccount.setOnClickListener {
            if (checkTermCondition.isChecked){
                val alertDialogView = LayoutInflater.from(this).inflate(R.layout.custom_dialog,null,false)
                val alertDialog = AlertDialog.Builder(this)
                alertDialog.setView(alertDialogView)
                alertDialogView.chekclist_animation.playAnimation()
                alertDialog.show()
                sharedPref?.edit()?.apply {
                    putString("username",editTextUsername.text.toString())
                    putString("email",editTextEmail.text.toString())
                    putString("password",editTextPassword.text.toString())
                    apply()
                }
                alertDialogView.btnOk.setOnClickListener {
                    val i = Intent(this,TermAndCondition::class.java)
                    startActivity(i)
                }

//                alertDialog.setTitle("Konfirmasi")
//                alertDialog.setMessage("Berhasil Registrasi")
//                alertDialog.setCancelable(false)
//                alertDialog.setPositiveButton("Ok"){ dialog, which ->
//                    dialog.dismiss()
//                    goToTermAndCondition()
//
//                }
//                alertDialog.setNegativeButton("Cancel"){ dialog, which ->
//                    dialog.dismiss()
//                }
//                alertDialog.show()


            }else{
//                Toast.makeText(this@RegisterActivity,getString(R.string.str_ceklist_term_condition),Toast.LENGTH_SHORT).show()

               val snackbar =  Snackbar.make(it,getString(R.string.str_ceklist_term_condition),Snackbar.LENGTH_INDEFINITE)


            }
        }


        myCalendar = Calendar.getInstance()
    }

    private fun goToTermAndCondition() {
        val i = Intent(this,TermAndCondition::class.java)
        i.putExtra("username",editTextUsername.text.toString())
        i.putExtra("email", editTextConfirmEmail.text.toString())
        i.putExtra("password",editTextPassword.text.toString())
        startActivity(i)
    }

    private fun onBirthdateSeleted() {
        val date = OnDateSetListener { view, year, monthYear, datOfMonth ->
            {
                myCalendar?.let {
                    myCalendar!![Calendar.YEAR] = year
                    myCalendar!![Calendar.MONTH] = monthYear
                    myCalendar!![Calendar.DAY_OF_MONTH] = datOfMonth
                }

            }
            updateLabel()
        }


        textBirthDate.setOnClickListener {
            myCalendar?.let {
               DatePickerDialog(this,date,myCalendar!!.get(Calendar.YEAR),
                    myCalendar!!.get(Calendar.MONTH),myCalendar!!.get(Calendar.DAY_OF_MONTH)).show()


            }

        }


    }


    override fun onStart() {
        super.onStart()

    }

    override fun onResume() {
        super.onResume()
    }


    override fun onPause() {
        super.onPause()
    }


    override fun onRestart() {
        super.onRestart()
    }

    override fun onStop() {
        super.onStop()
    }

    override fun onDestroy() {
        super.onDestroy()
    }

    private fun updateLabel() {
        val format = "MM/dd/yy"
        val sdf = SimpleDateFormat(format,Locale.US)

        textBirthDate.setText(sdf.format(myCalendar?.time))
    }

    private fun onSpinnerAttach() {
        val works = resources.getStringArray(R.array.works)

        val dataAdapter : ArrayAdapter<String> = ArrayAdapter(this,android.R.layout.simple_spinner_item,works)

        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

        spnrBtnJob.adapter = dataAdapter
    }

    private fun onRadioButtonCliked() {
        rdGroupGender.setOnCheckedChangeListener(object : RadioGroup.OnCheckedChangeListener{
            override fun onCheckedChanged(group: RadioGroup?, checkedId: Int) {
                when(checkedId){
                    R.id.rdBtnMan ->{
                      Toast.makeText(this@RegisterActivity,rdBtnMan.text,Toast.LENGTH_SHORT).show()
                    }
                    R.id.rdBtnWoman -> {
                      Toast.makeText(this@RegisterActivity,rdBtnWoman.text,Toast.LENGTH_LONG).show()
                    }
                }
            }

        })
    }


    override fun onNothingSelected(parent: AdapterView<*>?) {

    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {

    }

    companion object{

    }

}