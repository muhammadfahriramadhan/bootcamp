package com.sudomakmur.bootcamp

import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.sudomakmur.bootcamp.model.TaskEntity
import com.sudomakmur.bootcamp.room.TaskDatabase
import kotlinx.android.synthetic.main.activity_task.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async

class TaskActivity : AppCompatActivity() {

    private var mDBTask :  TaskDatabase? =null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_task)
        mDBTask = TaskDatabase.getInstance(this)



        setSupportActionBar(toolbar)
    }


    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater: MenuInflater = menuInflater
        inflater.inflate(R.menu.item_task, menu)
        return true

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.miSave -> {
                val title = edtxtTaskTitle.text.toString()
                val description = edtxDescription.text.toString()
                GlobalScope.async {
                    val result =  mDBTask?.TaskDao()?.insertTask(TaskEntity(null,title,description))
                    result?.let {
                        if (it != 0.toLong() ){
                            runOnUiThread {
                                edtxtTaskTitle.setText("")
                                edtxDescription.setText("")
                                Toast.makeText(this@TaskActivity,"Berhasil Input Data",Toast.LENGTH_LONG).show()
                            }

                        }
                    }
                }

                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}